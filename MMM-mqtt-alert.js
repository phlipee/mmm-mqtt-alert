'use strict';
/* global Module */

/* Magic Mirror
 * Module: MMM-mqtt-alert
 * 
 * By Phil Bias
 * MIT Licensed.
 * 
 * Based off MMM-mqtt
 * By Javier Ayala http://www.javierayala.com/
 */

Module.register('MMM-mqtt-alert', {

  defaults: {
    mqttServer: 'mqtt://test.mosquitto.org',
    mode: 'receive',
    topic: '',
    clearTopic: '',
    interval: 300000,
  },

  start: function() {
    Log.info('Starting module: ' + this.name);
    this.loaded = false;
    this.mqttVal = '';
    this.updateMqtt(this);
  },

  updateMqtt: function(self) {
    self.sendSocketNotification('MQTT_SERVER', { 
      mqttServer: self.config.mqttServer, 
      topic: self.config.topic, 
      clearTopic: self.config.clearTopic, 
      mode: self.config.mode 
    });
    setTimeout(self.updateMqtt, self.config.interval, self);
  },

  getDom: function() {
    var wrapper = document.createElement('div');

    return wrapper;
  },

  socketNotificationReceived: function(notification, payload) {
    if (notification === 'MQTT_DATA' && 
      (payload.topic === this.config.topic || payload.topic === this.config.clearTopic)) {
      this.mqttVal = payload.data.toString();
      this.loaded = true;
      this.updateDom();

      if (payload.topic === this.config.topic) {
        var data = JSON.parse(payload.data.toString());
        this.sendNotification("SHOW_ALERT", data);
      }
      else if (payload.topic === this.config.clearTopic) {
        this.sendNotification("HIDE_ALERT");
      }
    }

    if (notification === 'ERROR') {
      this.sendNotification('SHOW_ALERT', payload);
    }
  },

  notificationReceived: function(notification, payload, sender) {
    var self = this;

    if (self.config.mode !== "send") {
      return;
    }

    var topic;
    if (sender) {
      Log.log(this.name + " received a module notification: " + notification + " from sender: " + sender.name + ": ", payload);
      topic = this.config.topic + "/" + sender.name + "/" + notification;
    } else {
      Log.log(this.name + " received a system notification: " + notification + ": ", payload);
      topic = this.config.topic + "/" + notification;
    }

    this.sendSocketNotification("MQTT_SEND", {
      mqttServer: self.config.mqttServer,
      topic: topic,
      payload: payload
    });
  }

});
